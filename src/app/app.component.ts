import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'rock-paper-scissiors';

  private count = 0;
  private result = false;
  private resultDescription: string;
  private robotValue: string;
  private userValue: string;
  private entities: string[] = ['paper', 'scissors', 'rock'];

  playGames(value: string) {
    // const entity = this.entities[Math.round(3)];
    this.result = false;
    this.resultDescription = '';
    const min = 0;
    const max = 3;
    this.userValue = value;
    this.robotValue = this.entities[Math.floor((Math.random() * (3 - 0)) + 0)];
    this.resultDescription = this.getWinner(value, this.robotValue);
    console.log(this.resultDescription);
    console.log(this.userValue + '/' + this.robotValue);
    this.result = true;
  }

  getWinner(user: string, robot: string) {
    let result: string;
    if (user.match('paper')) {
      console.log('paper');
      switch (robot) {
        case 'paper':
          result = 'tie';
          break;
        case 'rock':
          result = 'win';
          break;
        case 'scissors':
          result = 'loose';
          break;
        default:
          result = 'abondon';
      }
    }
    if (user.match('rock')) {
      console.log('rock');
      switch (robot) {
        case 'paper':
          result = 'loose';
          break;
        case 'rock':
          result = 'tie';
          break;
        case 'scissors':
          result = 'win';
          break;
        default:
          result = 'abondon';
      }
    }
    if (user.match('scissors')) {
      console.log('scissors');
      switch (robot) {
        case 'paper':
          result = 'win';
          break;
        case 'rock':
          result = 'loose';
          break;
        case 'scissors':
          result = 'tie';
          break;
        default:
          result = 'abondon';
      }
    }
    return result;
  }
}
